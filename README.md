# qa-test-hf

**Description**
This test automation framework used to automate both frontend UI based as well as backend api based test cases.

**TESTCASES**

**UI based test cases**

*  Verify that user is successfully checking out new order
*  Verify that system is successfully creating user
*  Verify that user is successfully logging in to system

**Backend API based test cases**

*  Verify that api should restrict to book room more than once
*  Verify that api should restrict to book room with check in data is greater than checkout date
*  Verify that at least two existing bookings are returned
*  Verify that the data returned for an existing booking matches


**Prerequisites**
This framework requires following
*  Java 8
*  Maven

**Steps to execute**
1.  Go to the project root folder through cmd

    *  cd {Project-Path}

2.  Execute below command on cmd

    mvn clean test -Dbrowser={BROWSER-NAME} -Denv={ENVIRONMENT NAME}
    
**{BROWSER-NAME}**
This framework is supporting following browsers
*  firefox
*  chrome

**{ENVIRONMENT NAME}**
This framework is supporting following environments
*  dev1
*  dev2
*  uat
*  prod


**EXAMPLE COMMAND**
mvn clean test -Dbrowser=firefox -Denv=dev1

By default this framework will execute for **chrome** and **dev1** environment
To execute it on default parameters just execute the following command

mvn clean test

**LOGS**
This framework is generating logs on following path

**{user-home}/app/logs/test-logs.txt**

**REPORTING**

A detailed cucumber jvm report is being generated after every execution on following path

target/cucumber-JVM-reports/cucumber-html-reports/overview-features.html

**TAGGING**

There are two tags in this framework
*  @frontend 
    This tag is used for executing frontend UI based tests.
*  @backend
    This tags is used for executing backend api based tests.

Following command is used to execute test based on tagging

mvn clean test -Dbrowser=firefox -Denv=dev1 -Dcucumber.options="--tags '@frontend'"

if we donot give any tag with the command then it will execute all test cases

