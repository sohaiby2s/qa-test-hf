package com.test.challenge;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = {"com/test/challenge/frontend/stepdefinitions",
                "com/test/challenge/backend/stepdefinitions"},
        plugin = {"pretty", "html:target/cucumber-html-reports", "json:target/cucumber-reports.json"})
public class TestRunner {
}
