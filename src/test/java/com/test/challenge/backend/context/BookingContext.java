package com.test.challenge.backend.context;

import com.test.challenge.backend.model.Booking;
import com.test.challenge.backend.model.BookingDates;
import com.test.challenge.backend.model.BookingList;
import com.test.challenge.backend.restclient.HttpRestClient;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


@Component
public class BookingContext {

    private static final String REQUEST_JSON_PATH = "src/test/resources/data/json-model.json";
    private static final String GET_BOOKING = "/booking/";

    @Autowired
    private HttpRestClient httpRestClient;

    private BookingList bookingList;
    private Booking booking;

    public Response callGetBookingsApi() {
        Response response = httpRestClient.sendHttpRequest(Method.GET, GET_BOOKING);
        bookingList = response.getBody().as(BookingList.class);
        return response;
    }

    public Response callGetBookingApi(String bookingId) {
        Response response = httpRestClient.sendHttpRequest(Method.GET, GET_BOOKING + bookingId);
        booking = response.getBody().as(Booking.class);
        return response;
    }

    public Response callCreateBooking() throws IOException {
        httpRestClient.addHeader("content-type", "application/json");
        httpRestClient.setBody(booking);
        Response response = httpRestClient.sendHttpRequest(Method.POST, GET_BOOKING);
        return response;
    }

    public int getBookingListSize() {
        return bookingList.getBookings().size();
    }

    public Booking getBooking() {
        return booking;
    }

    public void setRoomId(int roomId) {
        booking.setRoomid(roomId);
    }

    public void setCheckInDate(String checkInDate, String checkOutDate) {
        BookingDates bookingDates = new BookingDates();
        bookingDates.setCheckin(checkInDate);
        bookingDates.setCheckout(checkOutDate);
        booking.setBookingdates(bookingDates);
    }

    public void createInitialBookingObject() throws IOException {
        byte[] bodyBytes = Files.readAllBytes(Paths.get(REQUEST_JSON_PATH));
        booking = new ObjectMapper().readValue(bodyBytes, Booking.class);
    }
}
