package com.test.challenge.backend.context;

import com.test.challenge.backend.restclient.HttpRestClient;
import io.restassured.path.json.JsonPath;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Data
public class CommonContext {

    @Autowired
    private HttpRestClient httpRestClient;

    private JsonPath responseBody;
    private Integer statusCode;

    public void setResponseBody(JsonPath responseBody) {
        this.responseBody = responseBody;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

}

