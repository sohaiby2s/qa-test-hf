package com.test.challenge.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Booking {
    private BookingDates bookingdates;
    private Integer bookingid;
    private Boolean depositpaid;
    private String email;
    private String firstname;
    private String lastname;
    private String phone;
    private Integer roomid;

}
