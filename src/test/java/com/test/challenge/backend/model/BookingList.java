package com.test.challenge.backend.model;


import lombok.*;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BookingList {

    private List<Booking> bookings;
}

