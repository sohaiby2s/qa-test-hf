package com.test.challenge.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreatedBooking {

    private Booking booking;
    private Integer bookingid;
}
