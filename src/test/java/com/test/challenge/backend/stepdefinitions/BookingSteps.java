package com.test.challenge.backend.stepdefinitions;

import com.test.challenge.backend.context.BookingContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class BookingSteps {

    @Autowired
    private BookingContext bookingContext;

    @When("User calls the get bookings api")
    public void whenUserCallsTheGetBookingApi() {
        bookingContext.callGetBookingsApi();
    }

    @When("User calls the get booking api for booking id {string}")
    public void userCallsTheGetBookingApi(String bookingId) {
        bookingContext.callGetBookingApi(bookingId);
    }

    @And("Api is successfully returning at least 2 bookings")
    public void apiIsSuccessfullyReturningAtLeastBookings() {
        Assert.assertTrue("Api is not showing last 2 bookings", bookingContext.getBookingListSize() >= 2);
    }

    @And("Api is successfully returning the data for booking id {int}")
    public void apiIsSuccessfullyReturningTheDataForBookingId(Integer bookingId) {
        Assert.assertEquals("Api is not returning the data for booking:" + bookingId
                , bookingContext.getBooking().getBookingid(), bookingId);
    }

    @Given("User has an initial Booking Data")
    public void userHasAnInitialBookingData() throws IOException {
        bookingContext.createInitialBookingObject();
    }

    @When("User calls the create bookings api")
    public void userCallsTheCreateBookingsApi() throws IOException {
        bookingContext.callCreateBooking();
    }

    @And("User sets the roomId to {int}")
    public void userCreatesABookingDataWithRoomId(int roomId) {
        bookingContext.setRoomId(roomId);
    }

    @And("User sets the check in date to {string} and check out date to {string}")
    public void userSetsTheCheckInDate(String checkInDate, String checkOutDate) {
        bookingContext.setCheckInDate(checkInDate, checkOutDate);
    }
}
