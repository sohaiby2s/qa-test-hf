package com.test.challenge.common;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.JsonNode;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public final class ConfigReader {

    private ConfigReader(){
    }

    public static final String URL = getConfigReader().get("url").asText();
    public static final  String API_URL = getConfigReader().get("apiURL").asText();
    public static final String EMAIL = getConfigReader().get("email").asText();
    public static final String PASSWORD = getConfigReader().get("password").asText();


    private static  JsonNode jsonNode;

    public static JsonNode getConfigReader() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            jsonNode = objectMapper.readTree(new File("src/test/resources/configurations/configurations.json"));
            jsonNode = jsonNode.get(getEnv());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonNode;
    }

    public static String getBrowser() {
        return StringUtils.isEmpty(System.getProperty("browser")) ? "chrome" : System.getProperty("browser") ;
    }

    public static String getEnv() {
        return StringUtils.isEmpty(System.getProperty("env")) ? "dev1" : System.getProperty("env") ;
    }

}
