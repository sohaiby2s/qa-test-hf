package com.test.challenge.frontend.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverBuilder {

    private static final Logger logger = LogManager.getLogger(WebDriverBuilder.class);

    public WebDriver buildWebDriver(String browserName) throws WebDriverException {

        switch (browserName.toUpperCase()) {
            case "CHROME":
                return getChromeDriver();
            case "FIREFOX":
                return getMozillaDriver();
            default:
                throw new WebDriverException();
        }
    }

    private WebDriver getChromeDriver() {
        logger.info("Getting Chrome web driver");
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

    private WebDriver getMozillaDriver() {
        logger.info("Getting Firefox web driver");
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }

}
