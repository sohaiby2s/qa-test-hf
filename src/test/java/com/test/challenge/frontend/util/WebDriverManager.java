package com.test.challenge.frontend.util;

import com.test.challenge.common.ConfigReader;
import io.cucumber.core.api.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

@Component
public class WebDriverManager {

    private static final Logger logger = LogManager.getLogger(WebDriverManager.class);

    private WebDriver driver;

    public WebDriver getWebDriver() {
        if (null == driver) {
            driver = createDriver();
        }
        return driver;
    }

    public void createScreenshot(Scenario scenario) {
        if (null != driver) {
            logger.info("Taking screenshots of failure scenario");
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
    }

    public void closeDriver() {
        if (null != driver) {
            logger.info("Closing web driver");
            driver.quit();
            driver = null;
        }
    }

    private WebDriver createDriver() {
        driver = new WebDriverBuilder().buildWebDriver(ConfigReader.getBrowser());
        driver.manage().window().maximize();
        return driver;
    }
}
