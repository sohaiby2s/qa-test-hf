package com.test.challenge.frontend.pages;

import com.test.challenge.frontend.pages.pageframe.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartProcessingPage extends PageBase {

    public static final String ORDER_COMPLETED_MESSAGE = "Your order on My Store is complete.";
    @FindBy(xpath = "//*[contains(@class,'cart_navigation')]/a[@title='Proceed to checkout']")
    private WebElement btnProceedToCheckOutSummary;

    @FindBy(name = "processAddress")
    private WebElement btnProceedToCheckOutAddress;

    @FindBy(id = "uniform-cgv")
    private WebElement checkTermsOfService;

    @FindBy(name = "processCarrier")
    private WebElement proceedToCheckOutShipping;

    @FindBy(className = "bankwire")
    private WebElement paymentType;

    @FindBy(xpath = "//*[@id='cart_navigation']/button")
    private WebElement btnConfirmOrder;

    @FindBy(css = "h1")
    private WebElement pageHeading;

    @FindBy(xpath = "//*[@class='cheque-indent']/strong")
    private WebElement orderCompletedText;

    @FindBy(xpath = "//li[@class='step_done step_done_last four']")
    private WebElement secondLastTab;

    @FindBy(xpath = "//li[@id='step_end' and @class='step_current last']")
    private WebElement lastTab;

    public ShoppingCartProcessingPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void clickBtnProceedToCheckOutSummary() {
        waitElementToBeVisible(btnProceedToCheckOutSummary);
        btnProceedToCheckOutSummary.click();
    }

    public void clickBtnProceedToCheckOutAddress() {
        waitElementToBeVisible(btnProceedToCheckOutAddress);
        btnProceedToCheckOutAddress.click();
    }

    public void clickCheckTermsOfService() {
        waitElementToBeVisible(checkTermsOfService);
        checkTermsOfService.click();
    }

    public void clickBtnProceedToCheckOutShipping() {
        waitElementToBeVisible(proceedToCheckOutShipping);
        proceedToCheckOutShipping.click();
    }

    public void clickPaymentType() {
        waitElementToBeVisible(paymentType);
        paymentType.click();
    }

    public void clickConfirmOrder() {
        waitElementToBeVisible(btnConfirmOrder);
        btnConfirmOrder.click();
    }

    public void waitTillOrderIsCompleted() {
        waitElementToBeVisible(pageHeading);
    }

    public String getPageHeadingText() {
        return pageHeading.getText();
    }

    public boolean isOrderCompletedSuccessfully() {
        return orderCompletedText.getText().equals(ORDER_COMPLETED_MESSAGE);
    }

    public boolean isCurrentPageIsTheLastPage() {
        return secondLastTab.isDisplayed() && lastTab.isDisplayed();
    }

}
