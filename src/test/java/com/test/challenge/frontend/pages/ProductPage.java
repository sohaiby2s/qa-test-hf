package com.test.challenge.frontend.pages;

import com.test.challenge.frontend.pages.pageframe.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage extends PageBase {

    @FindBy(name = "Submit")
    private WebElement btnAddToCart;

    @FindBy(xpath = "//*[@id='layer_cart']//a[@class and @title='Proceed to checkout']")
    private WebElement btnProceedToCheckOut;

    @FindBy(xpath = "//select[@id='group_1']")
    private WebElement selectSize;

    public ProductPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void clickBtnAddToCart() {
        waitElementToBeVisible(btnAddToCart);
        btnAddToCart.click();
    }

    public void clickBtnProceedToCheckOut() {
        waitElementToBeVisible(btnProceedToCheckOut);
        btnProceedToCheckOut.click();
    }
}
