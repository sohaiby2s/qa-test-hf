package com.test.challenge.frontend.pages.pageframe;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header extends PageBase {

    @FindBy(className = "login")
    private WebElement btnLogin;

    @FindBy(className = "account")
    private WebElement userName;

    @FindBy(className = "logout")
    private WebElement btnLogout;


    public Header(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void clickBtnLogin() {
        btnLogin.click();
    }

    public void selectFromMenu(String menuName){
        webDriver.findElement(By.linkText(menuName)).click();
    }

    public void waitLoginButtonToBeAppeared() {
        waitElementToBeVisible(btnLogin);
    }

    public String getUserName() {
        return userName.getText();
    }

    public boolean isHeaderContainsLogOut() {
        return btnLogout.isDisplayed();
    }
}
