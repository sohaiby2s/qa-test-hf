package com.test.challenge.frontend.pages;

import com.test.challenge.frontend.pages.pageframe.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

import static com.test.challenge.frontend.stepdefinitions.CreateAccountSteps.*;

public class CreateAnAccountPage extends PageBase {

    private static final int LENGTH = 6;

    @FindBy(id = "id_gender2")
    private WebElement radioMr;

    @FindBy(id = "customer_firstname")
    private WebElement txtCustomerFirstName;

    @FindBy(id = "customer_lastname")
    private WebElement txtCustomerLastName;

    @FindBy(id = "passwd")
    private WebElement txtPassword;

    @FindBy(id = "days")
    private WebElement selectDays;

    @FindBy(id = "months")
    private WebElement selectMonths;

    @FindBy(id = "years")
    private WebElement selectYears;

    @FindBy(id = "company")
    private WebElement txtCompany;

    @FindBy(id = "address1")
    private WebElement txtAddress;

    @FindBy(id = "address2")
    private WebElement txtAddressLine2;

    @FindBy(id = "city")
    private WebElement txtCity;

    @FindBy(id = "id_state")
    private WebElement selectState;

    @FindBy(id = "postcode")
    private WebElement txtZipPostalCode;

    @FindBy(id = "other")
    private WebElement txtAdditionalInformation;

    @FindBy(id = "phone")
    private WebElement txtHomePhone;

    @FindBy(id = "phone_mobile")
    private WebElement txtMobilePhone;

    @FindBy(id = "alias")
    private WebElement txtAddressAlias;

    @FindBy(id = "submitAccount")
    private WebElement btnRegister;


    public CreateAnAccountPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void fillPersonalInformation(Map<String,String> personalInfoData){
        waitElementToBeVisible(radioMr);
        radioMr.click();
        type(txtCustomerFirstName,personalInfoData.get(FIRST_NAME));
        type(txtCustomerLastName,personalInfoData.get(LAST_NAME));
        type(txtPassword,generateRandomString(LENGTH));
        selectByValue(selectDays,"1");
        selectByValue(selectMonths,"1");
        selectByValue(selectYears,"2000");
    }

    public void fillYourAddress(Map<String,String> addressData) {
        type(txtCompany, COMPANY_NAME);
        type(txtAddress, ADDRESS);
        type(txtAddressLine2, ADDRESS);
        type(txtCity, CITY);
        selectByVisibleText(selectState, addressData.get("state"));
        type(txtZipPostalCode, ZIP_CODE);
        type(txtAdditionalInformation, ADDITIONAL_TEXT);
        type(txtHomePhone, PHONE_NO);
        type(txtMobilePhone, PHONE_NO);
        type(txtAddressAlias, ADDRESS_ALIAS);
    }

    public void clickRegisterButton(){
        btnRegister.click();
    }

}
