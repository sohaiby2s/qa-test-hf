package com.test.challenge.frontend.pages;

import com.test.challenge.frontend.pages.pageframe.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends PageBase {

    public MainPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void clickProduct(String productName) {
        webDriver.findElement(
                By.xpath(String.format("//a[@class='product-name'][@title ='%s']", productName))).click();
    }

}
