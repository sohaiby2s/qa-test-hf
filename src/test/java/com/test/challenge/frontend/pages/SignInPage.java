package com.test.challenge.frontend.pages;

import com.test.challenge.frontend.pages.pageframe.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Date;

public class SignInPage extends PageBase {

    @FindBy(id = "email_create")
    private WebElement txtEmailAddress;

    @FindBy(id = "SubmitCreate")
    private WebElement createAnAccount;

    @FindBy(id = "email")
    private WebElement emailId;

    @FindBy(id = "passwd")
    private WebElement password;

    @FindBy(id = "SubmitLogin")
    private WebElement btnLogin;

    public SignInPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void setEmailAddress() {
        type(txtEmailAddress, createEmailAddress());
    }

    public void clickCreateAnAccount() {
        createAnAccount.click();
    }

    public void setEmailId(String txtEmail) {
        type(emailId, txtEmail);
    }

    public void setPassword(String txtPassword) {
        type(password, txtPassword);
    }

    public void clickBtnLogin() {
        btnLogin.click();
    }

    private String createEmailAddress() {
        String timestamp = String.valueOf(new Date().getTime());
        return String.format("hf_challenge_%s@hf%s.com", timestamp, timestamp.substring(7));
    }

    public void waitEmailCreateToBeVisible() {
        waitElementToBeVisible(txtEmailAddress);
    }
}
