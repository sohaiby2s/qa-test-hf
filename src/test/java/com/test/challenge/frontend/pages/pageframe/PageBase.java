package com.test.challenge.frontend.pages.pageframe;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;


public class PageBase {

    public static final String ALPHABETS = "abcdefghijklmnopqrstuvwxyz";
    protected WebDriver webDriver;
    protected WebDriverWait webDriverWait;
    protected Actions actions;
    protected Select select;

    public PageBase(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.webDriverWait = new WebDriverWait(this.webDriver, 10, 50);
        this.actions = new Actions(this.webDriver);
    }

    protected void waitElementToBeVisible(WebElement webElement) {
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
    }

    protected void selectByValue(WebElement webElement, String value) {
        select = new Select(webElement);
        select.selectByValue(value);
    }

    protected void selectByVisibleText(WebElement webElement, String value) {
        select = new Select(webElement);
        select.selectByVisibleText(value);
    }

    protected void type(WebElement webElement, String value) {
        webElement.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE);
        webElement.sendKeys(value);
    }

    protected String generateRandomString(int length) {
        StringBuilder randomString = new StringBuilder();
        Random rnd = new Random();
        while (randomString.length() < length) {
            int index = (int) (rnd.nextFloat() * ALPHABETS.length());
            randomString.append(ALPHABETS.charAt(index));
        }
        return randomString.toString();
    }

}

