package com.test.challenge.frontend.pages;

import com.test.challenge.frontend.pages.pageframe.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends PageBase {

    @FindBy(className = "navigation_page")
    private WebElement navigationPage;

    public MyAccountPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public boolean checkIfMyAccountPageOpened(String name) {
        return navigationPage.getText().equals(name);
    }

}
