package com.test.challenge.frontend.stepdefinitions;

import com.test.challenge.frontend.pages.ShoppingCartProcessingPage;
import com.test.challenge.frontend.util.WebDriverManager;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class ShoppingCartProcessingSteps {

    @Autowired
    private WebDriverManager webDriver;

    @When("I proceed to checkout on cart")
    public void iProceedToCheckout(){
        ShoppingCartProcessingPage shoppingCartProcessingPage = new ShoppingCartProcessingPage(webDriver.getWebDriver());
        shoppingCartProcessingPage.clickBtnProceedToCheckOutSummary();
        shoppingCartProcessingPage.clickBtnProceedToCheckOutAddress();
        shoppingCartProcessingPage.clickCheckTermsOfService();
        shoppingCartProcessingPage.clickBtnProceedToCheckOutShipping();
    }

    @When("I confirm the order by selecting payment method")
    public void iConfirmTheOrder(){
        ShoppingCartProcessingPage shoppingCartProcessingPage = new ShoppingCartProcessingPage(webDriver.getWebDriver());
        shoppingCartProcessingPage.clickPaymentType();
        shoppingCartProcessingPage.clickConfirmOrder();
    }

}
