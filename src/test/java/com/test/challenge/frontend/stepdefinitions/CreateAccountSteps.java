package com.test.challenge.frontend.stepdefinitions;

import com.test.challenge.frontend.pages.CreateAnAccountPage;
import com.test.challenge.frontend.pages.SignInPage;
import com.test.challenge.frontend.util.WebDriverManager;
import io.cucumber.java.en.And;

import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class CreateAccountSteps {

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String ZIP_CODE = "12345";
    public static final String PHONE_NO = "12345123123";
    public static final String COMPANY_NAME = "Warner Media";
    public static final String ADDRESS = "Test Address 1234";
    public static final String CITY = "Test City";
    public static final String ADDITIONAL_TEXT = "I am testing";
    public static final String ADDRESS_ALIAS = "AM";

    @Autowired
    private WebDriverManager webDriver;

    @And("I landed to the account creation page by entering unique email address")
    public void userEntersEmailAddressToCreateAnAccount() {
        SignInPage signInPage = new SignInPage(webDriver.getWebDriver());
        signInPage.waitEmailCreateToBeVisible();
        signInPage.setEmailAddress();
        signInPage.clickCreateAnAccount();
    }

    @When("I register new user by entering the following information")
    public void enterInformationInPersonalInfo(Map<String,String> userData){
        CreateAnAccountPage createAnAccountPage = new CreateAnAccountPage(webDriver.getWebDriver());
        createAnAccountPage.fillPersonalInformation(userData);
        createAnAccountPage.fillYourAddress(userData);
        createAnAccountPage.clickRegisterButton();
    }
}
