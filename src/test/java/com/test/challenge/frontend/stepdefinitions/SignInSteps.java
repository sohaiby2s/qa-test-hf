package com.test.challenge.frontend.stepdefinitions;

import com.test.challenge.common.ConfigReader;
import com.test.challenge.frontend.pages.SignInPage;
import com.test.challenge.frontend.pages.pageframe.Header;
import com.test.challenge.frontend.util.WebDriverManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class SignInSteps {

    @Autowired
    private WebDriverManager webDriver;

    @Given("I click on Sign in button")
    public void userClicksOnSignInButton() {
        Header header = new Header(webDriver.getWebDriver());
        header.waitLoginButtonToBeAppeared();
        header.clickBtnLogin();
    }

    @And("User enters email address in already registered block")
    public void userEnterEmailAddressInAlreadyRegisteredBlock() {
        SignInPage signInPage = new SignInPage(webDriver.getWebDriver());
        signInPage.waitEmailCreateToBeVisible();
        signInPage.setEmailId(ConfigReader.EMAIL);
    }

    @And("User enters password in already registered block")
    public void userEntersPasswordInAlreadyRegisteredBlock() {
        SignInPage signInPage = new SignInPage(webDriver.getWebDriver());
        signInPage.setPassword(ConfigReader.PASSWORD);
    }

    @When("I log into the application")
    public void userLogsInToTheSystem() {
        Header header = new Header(webDriver.getWebDriver());
        header.waitLoginButtonToBeAppeared();
        header.clickBtnLogin();
        SignInPage signInPage = new SignInPage(webDriver.getWebDriver());
        signInPage.waitEmailCreateToBeVisible();
        signInPage.setEmailId(ConfigReader.EMAIL);
        signInPage.setPassword(ConfigReader.PASSWORD);
        signInPage.clickBtnLogin();
    }

}
