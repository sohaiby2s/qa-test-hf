package com.test.challenge.frontend.stepdefinitions;

import com.test.challenge.frontend.pages.MyAccountPage;
import com.test.challenge.frontend.pages.pageframe.Header;
import com.test.challenge.frontend.util.WebDriverManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class MyAccountSteps {

    private static final String FIRST_NAME = "Sohaib";
    private static final String LAST_NAME = "Zahid";
    public static final String ACCOUNT_PAGE_ERROR_MESSAGE = "My account page is not opened";
    public static final String HEADER_PAGE_USER_ERROR_MESSAGE = "Header does not contain proper user name";
    public static final String HEADER_LOG_OUT_ERROR_MESSAGE = "Header does not contain logout";

    @Autowired
    private WebDriverManager webDriver;

    @Then("the user created successfully")
    public void theUserCreatedSuccessfully(){
        MyAccountPage myAccountPage = new MyAccountPage(webDriver.getWebDriver());
        Assert.assertTrue(ACCOUNT_PAGE_ERROR_MESSAGE, myAccountPage.checkIfMyAccountPageOpened("My account"));
    }

    @Then("the header should contain {word} and {word}")
    public void validateHeaderInfo(String firstName,String lastName){
        Header header = new Header(webDriver.getWebDriver());
        Assert.assertEquals(HEADER_PAGE_USER_ERROR_MESSAGE, String.format("%s %s",firstName,lastName)
                , header.getUserName());
    }

    @Then("{string} page is opened")
    public void pageIsOpened(String pageName) {
        MyAccountPage myAccountPage = new MyAccountPage(webDriver.getWebDriver());
        Assert.assertTrue(String.format("%s page is not opened",pageName),
                myAccountPage.checkIfMyAccountPageOpened(pageName));
    }

    @And("the username is shown in the header")
    public void userIsShownInTheHeader() {
        Header header = new Header(webDriver.getWebDriver());
        Assert.assertEquals(HEADER_PAGE_USER_ERROR_MESSAGE
                , header.getUserName(),String.format("%s %s",FIRST_NAME,LAST_NAME));
    }

    @And("Log out action is available")
    public void logOutActionIsAvailable() {
        Header header = new Header(webDriver.getWebDriver());
        Assert.assertTrue(HEADER_LOG_OUT_ERROR_MESSAGE, header.isHeaderContainsLogOut());
    }
}
