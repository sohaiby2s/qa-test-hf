package com.test.challenge.frontend.stepdefinitions;

import com.test.challenge.common.ConfigReader;
import com.test.challenge.frontend.util.WebDriverManager;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonSteps {

    @Autowired
    private WebDriverManager webDriver;

    @After
    public void afterScenario(Scenario scenario) {
        if (scenario.isFailed()) {
            webDriver.createScreenshot(scenario);
        }
        webDriver.closeDriver();
    }

    @Given("I am on the automation practice site")
    public void userIsOnAutomationPracticeSite() {
        webDriver.getWebDriver().get(ConfigReader.URL);
    }

}
