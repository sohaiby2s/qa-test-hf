package com.test.challenge.frontend.stepdefinitions;

import com.test.challenge.frontend.pages.MainPage;
import com.test.challenge.frontend.pages.ProductPage;
import com.test.challenge.frontend.pages.ShoppingCartProcessingPage;
import com.test.challenge.frontend.pages.pageframe.Header;
import com.test.challenge.frontend.util.WebDriverManager;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class ProductSteps {

    public static final String ORDER_CONFIRMATION_ERROR_MESSAGE = "Order Confirmation page is not loaded successfully";
    public static final String ORDER_CONFIRMATION = "ORDER CONFIRMATION";
    public static final String ORDER_CONFIRMATION_PAGE_ERROR_MESSAGE = "Url does not contain ?controller=order-confirmation";
    public static final String ORDER_COMPLETED_ERROR_MESSAGE = "Order is not completed successfully";
    public static final String CURRENT_PAGE_ERROR_MESSAGE = "Current Page is not the last page";
    @Autowired
    private WebDriverManager webDriver;

    @When("I add the following items to the cart")
    public void iConfirmTheOderBySelecting(Map<String,String> orderData){
        Header header = new Header(webDriver.getWebDriver());
        header.selectFromMenu(orderData.get("category"));
        MainPage mainPage = new MainPage(webDriver.getWebDriver());
        mainPage.clickProduct(orderData.get("product"));
        ProductPage productPage = new ProductPage(webDriver.getWebDriver());
        productPage.clickBtnAddToCart();
        productPage.clickBtnProceedToCheckOut();
    }

    @Then("the order is confirmed")
    public void validateIfTheOrderIsConfirmed(){
        ShoppingCartProcessingPage shoppingCartProcessingPage =
                new ShoppingCartProcessingPage(webDriver.getWebDriver());
        shoppingCartProcessingPage.waitTillOrderIsCompleted();
        Assert.assertEquals(ORDER_CONFIRMATION_ERROR_MESSAGE,
                ORDER_CONFIRMATION, shoppingCartProcessingPage.getPageHeadingText());
        Assert.assertTrue(ORDER_CONFIRMATION_PAGE_ERROR_MESSAGE
                , webDriver.getWebDriver().getCurrentUrl().contains("?controller=order-confirmation"));
        Assert.assertTrue(ORDER_COMPLETED_ERROR_MESSAGE,
                shoppingCartProcessingPage.isOrderCompletedSuccessfully());
        Assert.assertTrue(CURRENT_PAGE_ERROR_MESSAGE
                , shoppingCartProcessingPage.isCurrentPageIsTheLastPage());
    }
}
