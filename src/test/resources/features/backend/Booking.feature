@backend
Feature: Bookings

  Scenario: Verify that api should restrict to book room more than once
    Given User has an initial Booking Data
    And  User sets the roomId to 4
    And User calls the create bookings api
    And User calls the create bookings api
    Then Response code 409 is returned

  Scenario: Verify that api should restrict to book room with check in data is greater than checkout date
    Given User has an initial Booking Data
    And  User sets the check in date to "2022-02-26T22:20:13.875Z" and check out date to "2022-02-23T22:20:13.875Z"
    And User calls the create bookings api
    Then Response code 409 is returned

  Scenario: Verify that at least two existing bookings are returned
    When User calls the get bookings api
    Then Response code 200 is returned
    And Api is successfully returning at least 2 bookings

  Scenario: Verify that the data returned for an existing booking matches
    When User calls the get booking api for booking id "1"
    Then Response code 200 is returned
    And Api is successfully returning the data for booking id 1