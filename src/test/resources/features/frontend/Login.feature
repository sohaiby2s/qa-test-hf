@frontend
Feature: Login User

  Background:
    Given I am on the automation practice site

  Scenario: Verify that user is successfully logging in to system
    Given I am on the automation practice site
    When I log into the application
    Then "My account" page is opened
    And the username is shown in the header
    And Log out action is available