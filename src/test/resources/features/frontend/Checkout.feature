@frontend
Feature: Check out

    Background:
        Given I am on the automation practice site

    Scenario Outline: Verify that user is successfully checking out new order
        When I log into the application
        And I add the following items to the cart
        |category|<category>|
        |product |<product>  |
        And I proceed to checkout on cart
        And I confirm the order by selecting payment method
        Then the order is confirmed

        Examples:
        |category|product                           |
        |Women   |Faded Short Sleeve T-shirts       |
