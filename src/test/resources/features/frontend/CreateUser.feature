@frontend
Feature: User Creation

  Background:
    Given I am on the automation practice site

  Scenario Outline: Verify that system is successfully creating user
    When I click on Sign in button
    And I landed to the account creation page by entering unique email address
    And I register new user by entering the following information
      |firstName|<firstName>|
      |lastName |<lastName> |
      |state    |<state>    |
    Then the user created successfully
    And the header should contain <firstName> and <lastName>

    Examples:
    |firstName|lastName|state   |
    |first    |last    |Colorado|
